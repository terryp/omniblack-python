#! /usr/bin/python3
from os import environ
from enum import Enum
from typing import NamedTuple


class EnvTypes(Enum):
    home = 'home'
    work = 'work'


class Env(NamedTuple):
    type: EnvTypes
    headless: bool = False


headless = 'HEADLESS' in environ

current_env = environ.get('ENV', 'home')
env = Env(type=getattr(EnvTypes, current_env), headless=headless)
