Omniblack Env
*************

Get the curent enviroment for omniblack dotfiles projects.

At the moment it just checks for the env variable :code:`ENV` to determine
the enviroment type, and checks for the :code:`HEADLESS` to see if the
enviroment is headless.
