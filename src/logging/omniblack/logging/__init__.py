__version__ = '0.1.3'

__all__ = (
    '__version__',
    'config',
)

from .logConfig import config
