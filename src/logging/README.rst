Omniblack Logging
*****************

Configures logging to use a standard format and enables color output.

Color Output
************

Omniblack Logging supports color logging using
`Rich <https://github.com/willmcgugan/rich>`_.
Color output will be disabled when either :code:`NO_COLOR` or the
:code:`OMNIBLACK_NO_COLOR` enviroment variables. :code:`OMNIBLACK_NO_COLOR`
is custom to Omniblack Logging to disable our color output without
disable color output for other programs.
