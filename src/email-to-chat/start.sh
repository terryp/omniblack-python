#! /usr/bin/sh

. /code/.venv/bin/activate
export PYTHONPATH="${PYTHONPATH}:/code"
exec python -m omniblack.email_to_chat
