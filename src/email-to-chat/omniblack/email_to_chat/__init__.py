__all__ = (
    'run_email',
)

__pkg_name__ = 'omniblack.email_to_chat'
from .__main__ import run_email

