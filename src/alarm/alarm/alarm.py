#! /usr/bin/python
"""
Run alarms at a specified time.
"""

import asyncio
from datetime import date, time, datetime, timedelta
from itertools import count
from logging import info, DEBUG
from program import register_main
from notify import notify

from config import config_file, watch_files


text_weekday = {
    'monday': 0,
    'tuesday': 1,
    'wednesday': 2,
    'thursday': 3,
    'friday': 4,
    'saturday': 5,
    'sunday': 6,
}

days = text_weekday.keys()


def convert_days(days):
    return {text_weekday[day.lower()] for day in days}


def acceptable_dates(days):
    days = convert_days(days)
    for offset in count(0):
        next_date = date.today() + timedelta(days=offset)
        week_day = next_date.weekday()
        if week_day in days:
            yield next_date


running_tasks = {}


def find_date(alarm):
    now = datetime.now()

    for day in acceptable_dates(alarm.days):
        alarm_date = datetime.combine(day, alarm.time)
        if alarm_date > now:
            return alarm_date


async def execute_alarm(alarm):
    notify(f'Alarm at {alarm.time}', message=alarm.title)


async def schedule_alarm(alarm):
    while True:
        next_date = find_date(alarm)
        now = datetime.now()
        delta = next_date - now
        seconds = delta.total_seconds()
        info(f'Scheduled "{alarm.title}" at {next_date.isoformat()}')
        await asyncio.sleep(seconds)
        await execute_alarm(alarm)


@register_main(level=DEBUG)
async def create_alarms(test: int = False):
    """
        Args:
            test: A test Parameter.
    """
    info('Started')
    async for configs in watch_files(config_file('alarms')):
        info('Reloading config')
        for task_id, task in running_tasks.copy().items():
            task.cancel()
            del running_tasks[task_id]

        for alarm_id, alarm in enumerate(configs.alarms):
            alarm.id = alarm_id
            alarm.time = time(*map(
                lambda time_str: int(time_str),
                alarm.time.split(':'),
            ))

            task = asyncio.create_task(schedule_alarm(alarm))
            running_tasks[alarm_id] = task
