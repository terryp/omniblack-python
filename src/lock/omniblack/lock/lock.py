from anyio import Lock as AsyncLock
from anyio.from_thread import run, run_sync


class Lock:
    def __init__(self):
        self.__lock = AsyncLock()

    def __enter__(self):
        run(self.__lock.acquire)

    def __exit__(self, exc_type, exc_value, traceback):
        run_sync(self.__lock.release)

    async def __aenter__(self):
        await self.__lock.acquire()

    async def __aexit__(self, exc_type, exc_value, traceback):
        self.__lock.release()
