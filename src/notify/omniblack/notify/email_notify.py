from email.message import EmailMessage
from email.policy import SMTP as SMTPPolicy, strict
from smtplib import SMTP
from getpass import getuser
from config import program_name
from atexit import register

policy = SMTPPolicy + strict

user_email = f'{getuser()}@localhost'
sending_email = f'{program_name}@localhost'

smtp = SMTP('localhost')
register(smtp.quit)


def notify(title, message):
    msg = EmailMessage(policy=policy)
    msg.set_content(message)
    msg['Subject'] = title
    msg['From'] = sending_email
    msg['To'] = user_email
    smtp.send_message(msg)

