from env import env

if env.headless:
    from email_notify import notify
else:
    from gui_notify import notify
