from tkinter import Tk
from tkinter.messagebox import showwarning, showinfo
from enum import Enum


ROOT = Tk()
ROOT.withdraw()


def notify(title, message):
    showwarning(title, message)
    ROOT.update()
